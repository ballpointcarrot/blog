+++
title = "Dailies"
date = 2019-03-25
slug = "dailies"

[taxonomies]
tags = ["Metablog"]

[extra]
uuid = "92bd98d0-292a-4ed0-bb0a-d2ca16b5a302"
date-published = 2019-03-25
in-language = "en"
keywords = ["blog", "frequency", "daily", "checklist"]
+++
I realized I hadn't updated this space in a fair amount of time. The Advent of Code project, while fun, also became very time-consuming (if you add in the time spent around mid- to late-December performing in choir events, I ran out of time to do much of anything).

However, I'm going to try to hold myself to updating this at least somewhat regularly. In order to help me do that, I've put together a "dailies" section, where I can drop a daily thought. That will be presented on the main page sidebar, and should hopefully hold me to updating it (even with a trivial thought) once a day. 
<!-- more -->

I'll like the churn on the git repo as well, but that's just a pleasant side-effect.

The dailies should be live as of this post (see [dailies](https://www.ballpointcarrot.net/dailies)) - They will also be paginated, so don't worry about having thousands of things hit you at once. Fortunately, [Zola](https://www.getzola.org/), my static site generator, handles that really nicely.


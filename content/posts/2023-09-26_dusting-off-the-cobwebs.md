+++
title = "Dusting off the Cobwebs"
slug = "dusting-off-the-cobwebs"

[taxonomies]
tags = ["Metablog"]
+++

I've been meaning to come back to this space and give it the standard periodic cleaning (as [has been done](/posts/im-still-here) in [the past](/posts/well-i-shouldve-been) a [couple of times](/posts/yearly-refresh-time)).

## What's changed?

Since the last post in 2021 (which I'm super proud of, it was the
best-performing post I've had), I've helped raise a kid to 2 years old (he was
1 month old when that last post landed), changed jobs (went through a layoff,
and found new employment elsewhere); my primary social media platform was
purchased by a person with more money and ego than sense, and has turned it
into a cesspool of fascist propaganda and hate speech; and the world has
continued dealing with the continuing COVID
pandemic. 

## No, I mean what's changed on the *site*?

Oh, right.

Well, the very obvious one is there's a new style and theme attached. Gone is
the smooth lines and colorful graphics, and now we have a simple text-forward
site. *This is by choice.* I've stumbled down a rabbit hole of blog posts
containing concerns that "web pages are too large", and some [satirical and
expletive-laden](https://www.reddit.com/r/webdev/comments/mkewwn/this_motherfucking_website_no_seriously_a_must/gtfjtrt/)
examples to the contrary.

The overall sense that I got from the above is that, the less amount of
unnecessary data being requested by your users, the better. So, the
simplification of this site tracks with that goal. We have a simple font
selection set (which will render mostly the same); the colors are inspired by
the Catppuccin color theme (which I'm using all over my tools, it's seriously
great), and I've tried to limit image usage to only the necessary places.

## What's next?

Being a parent sucks a lot of the free time away from tinkering, so likely not
a ton. That said, I've admired what [Ben Werdmuller](https://werd.io) does, and
I've been a fan of the Indieweb movement in general, so I'll likely do some work to 
enable more of this site to use Indieweb-related functionality. I think I've
taken a bit of a step back with this recent change, and so I'll do some cleanup
in the near term to get be back to some level of microformats usage (right now,
I'm just wanting to get the style change out the door, and I'll iterate on that
in later days).

And hey, [Advent of Code](/tags/advent-of-code) comes back here in December, so I'll maybe have some
things to write there. Gonna have a think on which language to use...

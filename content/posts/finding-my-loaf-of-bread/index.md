+++
title = "Finding my Loaf of Bread"
date = 2019-05-07
slug = "finding-my-loaf-of-bread"

[taxonomies]
tags = ["Programming", "Baking", "Learning"]

[extra]
uuid = "63f224d1-a2bc-41ff-86c6-49b454ebce86"
date-published = 2019-05-07
in-language = "en"
keywords = ["bread", "coding", "projects", "learning"]
+++
I made my first loaf of bread last Saturday.

I've been watching a lot of ["The Great British Bakeoff"](https://thegreatbritishbakeoff.co.uk/) (or "Baking Show" if you're in the US, because of [trademark reasons](http://www.pbs.org/publiceditor/blogs/pbs-public-editor/whats-in-a-name/)). Watching people on your television make intricate, creative, and most importantly *delicious-looking* baked goods, the desire to try it yourself becomes irresistable. Both my wife and I have tried some things we've seen on the show, to decent result. However, I've never really been a "Baker" - I've generally avoided the big hot box that sits underneath the stove, and would tell people that me and that box didn't really get along well.

Despite this, the drive to try it out was too much to handle. I made use of the fact that my wife was out of town this past weekend to give me some buffer time: if things turned out well, I'd have a nice surprise for her when she returned; if I screwed up things or made a complete mess of the kitchen, I'd have time to destroy the evidence. :D

<!-- more -->

My project took about 3 hours from start to finish; I began later than I really should have (around 8pm), but since I wasn't disturbing anyone by my late-night "breadscapade", I kept at it. I won't go into too much of the process, as photos can provide more insight here:

The dough sat for about an hour to rise:

![pre-rise](IMG_20190504_210312.jpg)

![post-rise](IMG_20190504_220141.jpg)

After rising, the recipe I was following directed me to braid 3 strands of the loaf together:

![braided, but unbaked](IMG_20190504_221614.jpg)

Then, after an egg wash, it was in the oven for a half-hour. The end result:

![glorious looking bread](IMG_20190504_225522.jpg)

Needless to say, I'm happy with how everything turned out.

Now, this is being posted on [dev.to](https://dev.to/), so what does *any* of this have to do with programming?

Reflecting on the process I had doing the above: the timeframe it took, the level of familiarity, determining what I could take away from the experience for next time - I began to wonder what my programming "Loaf of bread" is. Reviewing the criteria I had:

* took about 3 hours start-to-finish
* dealt with something unfamiliar
* a safe way to clean up the mess in case it failed
* the ability to [chronicle the process](https://twitter.com/ballpointcarrot/status/1124895392288477184)
* I can share it with others if it turns out well
* I should have fun doing it

For me, the hardest thing in converting this metaphor to programming is the time slot. Learning something new in the programming world (new language/framework/technique/etc.) takes a signifigantly longer amount of time, and I'm generally the type to read through and find tutorials and materials to get a sufficient level of understanding before trying to crank something out on my own. Taking only a 3h timeslot and trying to do learn something meaningful in a new language seems impossible to fit. Maybe I need to expand the time. I could reduce the scope on what I'm learning, but then I feel like I don't capture enough to make what I've learned meaningful with just a tiny piece. This is my normal problem with katas - I can figure out how to solve the problem, but it feels external to how you would deal with a "real problem" in that language.

I also struggle with a aversion to failure - that's why I had to make sure I had time alone to try the bread, and have sufficient time in case of disaster. I struggle with wanting to do cool things, but not wanting to mess up things in the process. Even though I consciously know that no software project ever works this way, I subconsiously want the thing I build to have a well-thought-out design, and be more-or-less right the first time. Software has the remarkable ability to evolve; I just need to learn to embrace it. With the bread, if I screwed it up I could toss it out, and nobody would know.

I'm working on ways to handle tracking progress when building something new. I'm usually willing to post in-progress things up on Twitter or the Fediverse, and used some free time on the weekend to set up everything I needed to run livecoding sessions on Twitch. However, there's a big jump between preparing to do that, and actually pulling the trigger.

An example: I've got a project that I've had for a year that I have started a number of times, but never actually created anything meaningful on. In the latest incantation, I was working on it using Elixir (my language to learn for the year, which I'm doing a bad job of considering it's almost half-over). Once I had my livestream stuff set up, I became terribly nervous - I don't know this language, so the majority of the stream is just going to be me reading through docs, which won't be terribly interesting. I over-indexed on this, and then ended up deciding to just move on to the next household project instead of working on it.

I'm inside my head a lot here - In my case, I wouldn't have anyone watching to begin with, so streaming out to the void and reading docs may have been okay. And hey, maybe someone jumping in could assist.

I'd be interested to hear any of your coding "loaves of bread". What kind of projects do you use for trying things out? Is it worth doing a bigger-scope, longer-term thing to build up knowledge? Should I alter my scope of what I consider a "real problem" for a language, so that I can just do?

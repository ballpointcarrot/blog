+++
title = "Daily: 2019-03-26"
date = 2019-03-26
slug = "2019-03-26"

[taxonomies]
tags = ["daily"]

[extra]
uuid = "4af7c128-0bfc-489d-b5fe-d5972622becb"
date-published = 2019-03-26
+++
Realizing more and more that, while I'm good at putting together demos and trying out new things, mastery is still escaping me. I want to grab some small problems and start building some more knowledge depth.

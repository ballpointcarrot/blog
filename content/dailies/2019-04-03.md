+++
title = "Daily: 2019-04-03"
date = 2019-04-03
slug = "2019-04-03"

[taxonomies]
tags = ["daily"]

[extra]
uuid = "7ae58ea4-902c-455e-a434-7376e48c4f76"
date-published = 2019-04-03
+++
Just put all of my choir stuff for the next two weeks on the calendar. I may spend more time at the Cathedral than I do at home over the week leading up to Easter.

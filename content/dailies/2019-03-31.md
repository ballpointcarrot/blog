+++
title = "Daily: 2019-03-31"
date = 2019-03-31
slug = "2019-03-31"

[taxonomies]
tags = ["daily"]

[extra]
uuid = "5f069c37-00d3-41d1-ade7-3062a33e6f13"
date-published = 2019-03-31
+++
Knocked out a few leetcode problems today. Relied pretty heavily on the Rust docs while doing it, but that's because I don't have the Rust stdlib under my belt yet.

Results were better than expected, although I took longer to get to solutions than I wanted. Anyway, forward progress.

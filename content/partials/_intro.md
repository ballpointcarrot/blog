+++
in_search_index = false
+++

## Intro

Oh hi.

I'm Christopher Kruse, a software developer and all-around nerd living around
Seattle, WA, USA. I like to spend my time in JS/TS, Clojure and Rust when
possible. When I'm not at the keyboard, I'm largely found singing [with
choirs](https://summerfling.org), on the
[Fediverse](https://fedi.bpc.wtf/@ballpointcarrot), or playing some video
games.

+++
title = "/uses/"
date = "2023-09-26"
updated = "2023-10-31"
+++

This is a "uses" page, which details what I use on a daily basis in my work as
a developer and general computing nerd, as well as other tools, utilities and
resources that I find handy and wish to share with others. The concept was
popularized by [uses.tech](https://uses.tech).

I try to keep this list current. In order to keep myself honest, I'll display the last updated date here, so I can be 
publicly ridiculed when I get out of date.

> Last updated: {{ last_updated() }}


## Hardware

| Class | Thing | Used for |
|-------|-------|----------|
| Personal Laptop | Lenovo Thinkpad T14s Gen 2 | Code, Web browsing, Primary "Compute" system |
| Other laptop | Macbook Pro M1 | Was laid off, but company let me keep it. Now a media device mostly. |
| Desktop | Custom built, AMD Ryzen 3800x | Gaming Home Theater PC |
| Mobile | OnePlus 11 5G (CPH2451) | Doomscrolling and wasting time |
| Keyboard | [Keychron Q8 Alice](https://www.keychron.com/pages/keychron-q8-customizable-mechanical-keyboard) with [MELETRIX WS Grey](https://www.amazon.com/gp/product/B0BHWK8CHW/) switches. | A **lot** of typing. |
| Over-the-ear Headphones | Audio-Technica ATH-M50x | Conference calls, Music listening|
| In-ear Headphones | Moondrop Kato | "sit and listen" music sessions | 
| Audio Amplifier | HIBY FC4 | Used mostly with the IEMs, "sit and listen" music |

## Software
| Class | Tool of Choice | Notes |
|-------|--------------|--------|
| Personal Laptop OS | [EndeavourOS](https://endeavouros.com) | Love the Arch ecosystem, but not the install process. |
| Display Manager | [KDE](https://www.kde.org) | Miss using a tiling WM, but also happy to have creature comforts baked in. |
| Text Editor | [NeoVim](https://neovim.io) | Making heavy use of the [LazyVim](https://www.lazyvim.org) starter. I can never solidly decide between vim and emacs. |
| Web Browser | [Vivaldi](https://vivaldi.com) | Love having tree-view tabs on the left side of the screen. |
| [Shell](https://en.wikipedia.org/wiki/Shell_%28computing%29) | [fish](https://fishshell.com) | Decorated with [Starship](https://starship.rs). Was a long-time ZSH user, but the auto-complete of fish tempted me. |
| Notes | [LogSeq](https://logseq.com) | Started putting my notes here, Syncing between my devices with [Syncthing](https://syncthing.net).|

+++
render = true
template = "resume.html"
+++

# Christopher Kruse

[LinkedIn](https://linkedin.com/in/krusec)
[GitHub](https://github.com/ballpointcarrot)
[Email](mailto:ckruse@ballpointcarrot.net)

## Work Experience

**Moveworks ::** Remote<br />
*Senior Software Engineer, DevOps*<br />
April 2024 - Present

- Drove CI build time improvements through Bazel cache configuration
- Assisted in large-scale migration of codebase to updated versions of Python, Bazel and protobuf
- Optimized the new hire onboarding experience, with a unified, idempotent onboarding script for both OSX and Ubuntu


**Autodesk (Contract via Capstone Staffing) ::** Remote<br />
*Development Ops III*<br />
December 2023 - April 2024

- Drove migrations from existing Terraform into an AWS CDK-based SDLC tool called "Menu Card"
- Quickly became part of the Menu Card core team, and helped drive new features into the product:
  - Built custom Constructs to wrap around AWS resource definitions to predefine organization best practices
  - Fleshed out features around Blue/Green deployment, integration testing between deployment stages

**Flexport ::** Bellevue, WA<br />
*Senior DevOps Engineer*<br />
August 2022 - October 2023

- Owned and lead development for our software catalog and developer tools platform built on Backstage
  - Promoted a Self-service registration to the platform, with ~150 services registered
  - Promoted deeper use of the catalog features, like CI/CD visualization and hosted documentation
  - API-powered automatic dependency tracking for incident processes
- Maintained a suite of GitHub Actions and reusable workflows to streamline steps in the CI/CD process for services
- Aided in launching Argo CD for managing Kubernetes deployments using GitOps-style deployment management
- Managed and maintained the internal GitHub Enterprise Server and JFrog Artifactory installations

**Stedi ::** Remote<br />
*Engineering*<br />
February 2020 - August 2022

- Built a custom CI/CD pipeline for AWS CDK via AWS CodePipeline and CodeBuild
- Developed internal tooling to log and track privilege access requests to restricted environments
- Built multiple revisions of parsers for EDI data, learning from each implementation how to generalize the parser, casting an ever-wider net for customer usage patterns
- Helped foster a culture of toil reduction, driving to automate repetitive, manual tasks
- Participated in code reviews, helping improve the quality of the delivered products
- Built a mechanism for automatically applying semantic versioning of package releases, while maintaining branch protection for the main branch of our repositories
- Created build and deploy scripts across several packages, introducing performance improvements and lowering build and deploy times

**Amazon Web Services ::** Seattle, WA<br />
*Systems Development Engineer*<br />
December 2017 - January 2020

- Led the design and development of a performance management dashboard for AWS Customer Support
  - Replaced an ad-hoc process of spreadsheets and 1-1 manager discussions
  - Systemizing the process led to agents being able to use their performance metrics as part of their promotion discussions
- Served on incubation team for Support Enigineer-created projects
  - took ideas from people in the field, productionalized them and made them available throughout the team.
- Mentored junior members of the team as tech lead

*Solutions Architect, World-wide Public Sector SA team*<br />
August 2016 - December 2017

- Built out a discount strategy plan for promoting open data sharing
- Worked with external customers to design application architectures to fit the AWS Cloud
- Created sample applications providing demonstrations of specific technologies
- Ran overviews and deep dive presentations of AWS Services
- Lead group hands-on training sessions for AWS Services
- Provided in-depth architecture review for customers; focused on security, performance, reliability and cost optimization

*Systems Development Engineer, Elastic Beanstalk*<br />
December 2014 - August 2016

- Designed and implemented a CI/CD system for building Elastic Beanstalk environment stacks, supporting Java/Tomcat, Python, PHP, Ruby, Docker and NodeJS
- Managed software deployment and testing lifecycle operations, including parallel deployments to multiple geographic regions
- Requested, set up and administered backend hosts for service extensions
- Instituted regular quality reviews and internal tooling updates
- Provided assistance to the Customer Support team for in-depth questions regarding the service
- Routed and assisted in resolving service-level issues, creating workarounds for customers and eliminating bugs found in the service

*Developer Support Engineer*<br />
February 2013 - December 2014

- Provided phone, email and chat-based support to customers of AWS Services
- Created training materials for and internal classes for coworkers on CloudFormation and Elastic Beanstalk services
- Created Subject Matter Expert (SME) programs for CloudFormation, Elastic Beanstalk, OpsWorks and Simple Workflow services
- Selected as an SME for CloudFormation, Elastic Beanstalk and Simple Workflow
- Worked with service teams to reduce contact escalation rates, and to provide improvements to customers of the services
- Developed and maintained utilities used by the Support organization, to make the day-to-day work easier
- Worked with the Customer Support Development team to provide long-lasting improvements to the Support organization

**SR Education Group ::** Kirkland, WA<br />
*Web Developer*<br />
June 2011 - January 2013

- Created features for a set of websites built for online college lead generation using Ruby on Rails
- Led impromptu code reviews with coworkers; helped streamline and improve team members' code
- Managed a fleet of micro-websites on EC2/Nginx as proxy/caching servers
- Provided reporting data via third-party APIs to help influence the project's direction

## Skills

- Programming Languages:
  - Typescript
  - Rust
  - Ruby
  - Java
- Cloud Native:
  - Infrastructure as Code (AWS CDK, AWS CloudFormation, Terraform)
  - AWS Serverless (Lambda, API Gateway, DynamoDB, S3), CodeBuild and CodeDeploy
  - Kubernetes, Helm and Kustomize, Argo CD, Backstage
- Tools:
  - git, docker, Ruby on Rails, PostgreSQL 

## Education and Certifications
- **Bachelor of Science (Cum Laude), Computer Science**<br />
  Clarke University, Dubuque, IA <br />
  May 2009

## Other

- Languages: English (native), Japanese (beginner)

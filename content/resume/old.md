+++
render = true
template = "resume.html"
+++

## Vision

<p class="center">Use my presence to propel a solid team of developers to release great software. I bring a healthy curiosity, a drive to improve the day-to-day life of the engineers around me, and a high bar for quality to any team I work with.</p>

## Skills and Projects

- Programming Languages:
  - Javascript/Typescript: Node and NPM backend services and scripts, JSON Schema-based service definition, Browser automation, some frontend work (React, Vue)
  - Rust: Service development, targeted to CPU-bound processing
  - Ruby: Rails/Sinatra web applications, Rake build scripting
  - Java: Application design and development, Data Load/ETL Processing and Report Generation, Servlets/JSP, Ant and Maven project management and build automation
  - Clojure/CLJS: Personal projects
- Cloud Services:
  - AWS: Serverless application development (Lambda + API Gateway + DynamoDB + S3), CloudWatch + Xray, CloudFormation, CodeBuild + CodeDeploy, EC2, Step Functions 
  - Kubernetes: Cluster access and application deployment via kubectl, helm and Kustomize
- Software Tools/Libraries:
  - Web Frameworks: Ruby on Rails, Sinatra, Ring/Compojure, Laravel, Django
  - Utility libraries: AWS SDK, jQuery, underscore/lodash
  - Build Tools: AWS CDK, just, NPM, Rake, Ant, Maven, Boot, Leiningen, Grunt, webpack
  - CI/CD tools: GitHub Actions, AWS CodeBuild + CodeDeploy, Hudson/Jenkins 
  - Configuration Management: AWS CDK, Docker, Chef, Ansible
  - Databases: DynamoDB, MySQL, PostgreSQL, MongoDB
  - Source Control: git, subversion
- Projects:
  - [ballpointcarrot.net](https://www.ballpointcarrot.net) : Personal Website/Blog; [JAMstack](https://jamstack.org) website generated by [Zola](https://www.getzola.org) and lives on [Netlify](https://www.netlify.com/).

## Work History

**Senior DevOps Engineer** - Flexport, Bellevue, WA <br />
August 2022 - Present

- Launched a software catalog and developer tools platform called Backstage to help teams expose service details and metadata to the rest of the organization
- Managed complex GitHub actions workflows used to provide CI/CD pipelines and provision resources as part of project generation; designing ways to reduce this complexity
- Designing a new process for handling managed software configuration and updates for software projects; helping address the problem of "how do we remove the tedious setup and SDLC processes so developers can focus on their domain?"
- Manage and maintain the internal GitHub Enterprise Server and JFrog Artifactory installations

**Engineering** - Stedi, Remote <br />
February 2020 - August 2022

- Built a custom deployment pipeline for AWS CDK via AWS CodePipeline and CodeBuild (prior to the release of CDK Pipelines)
- Developed internal tooling to help with content sharing between teams, and log and track privilege access requests to restricted environments
- Served as a reference point for EDI usage, having used it in the healthcare field
- Built multiple revisions of parsers for EDI data, learning from each implementation how to generalize the parser, casting an ever-wider net for customer usage patterns
- Helped foster a culture of toil reduction, driving to automate repetitive, manual tasks
- Participated in code reviews, helping improve the quality of the delivered products
- Built a mechanism for handling semantic versioning of package releases, while maintaining branch protection for the main branch of our repositories
- Created build and deploy scripts across several packages, introducing performance improvements and lowering build and deploy times

**Systems Development Engineer** - Amazon Web Services, Seattle, WA <br />
December 2017 - January 2020

- Designed and created a web portal for tracking Support Agent metrics and performance over time
- Developed internal applications to improve the AWS Support experience
- Maintained and improved existing Support Engineer-created scripts and applications
- Mentored junior members of the team as tech lead

**Solutions Architect** - Amazon Web Services, Seattle, WA <br />
August 2016 - December 2017

- Worked with external customers to design application architectures to fit the AWS Cloud
- Built internal tooling for the Solutions Architecture team
- Created demo applications providing demonstrations of specific technologies
- Ran overviews and deep dive presentations of AWS Services
- Lead group hands-on training sessions for AWS Services
- Provided in-depth architecture review for customers; focused on security, performance, reliability and cost optimization

**Systems Development Engineer, Elastic Beanstalk** - Amazon Web Services, Seattle, WA <br />
December 2014 - August 2016

- Designed and implemented a CI/CD system for building Elastic Beanstalk software stacks, supporting Java/Tomcat, Python, PHP, Ruby, Docker and Node.js
- Managed software deployment and testing lifecycle operations, including parallel deployments to multiple geographic regions
- Requested, set up and administered backend hosts for service extensions
- Instituted regular quality reviews and internal tooling updates
- Provided assistance to the Customer Support team for in-depth questions regarding the service
- Routed and assisted in resolving service-level issues, creating workarounds for customers and eliminating bugs found in the service
- Participated in in-depth code reviews with members of the service team

**Developer Support Engineer** - Amazon Web Services, Seattle, WA <br />
February 2013 - December 2014

- Provided phone, email and chat-based support to customers of AWS Services
- Created training materials for and internal classes for coworkers on CloudFormation and Elastic Beanstalk services
- Created Subject Matter Expert (SME) programs for CloudFormation, Elastic Beanstalk, OpsWorks and Simple Workflow services
- Selected as an SME for CloudFormation, Elastic Beanstalk and Simple Workflow
- Worked with service teams to reduce contact escalation rates, and to provide improvements to customers of the services
- Developed and maintained utilities used by the Support organization, to make the day-to-day work easier
- Worked with the Customer Support Development team to provide long-lasting improvements to the Support organization

**Web Developer** - SR Education Group, Kirkland, WA <br />
June 2011 - January 2013

- Created features for a set of websites built for online college lead generation using Ruby on Rails
- Led impromptu code reviews with coworkers; helped streamline and improve team members' code
- Managed a fleet of micro-websites on EC2/Nginx as proxy-caching servers
- Provided reporting data via third-party APIs to help influence the project's direction

**Software Engineer** - RelayHealth, Dubuque, IA <br />
March 2010 - May 2011

- Implement and customized the Payer Connectivity Services medical claims processing SaaS platform
- Developed custom applications in Java, Python and C#/.NET to enhance the claims processing workflow for individual customers
- Organized and educated coworkers on Agile methodologies towards software implementation
- Installed and maintained a Hudson bulid server for automated build and deployment
- Created an analytics reporting system in Java with JasperReports for a daily clam count audit, including claims aging and routing logic

**Software Engineer** - Sysorex Federal, Inc., Rock Island, IL <br />
August 2009 - November 2010

- Supported and upgraded the Munitions Transport Management System (MTMS), a Java-based tracking database frontend designed to maintain the chain of custody for munitions shipments
- Performed usability and performance upgrades to the existing system; redesigned a major portion of the UI on the desktop client, and rewrote old existing Servlets to data-fed JSP pages
- Refactored old Java 1.1-era code to Java 5+ Standards (including adding support for Generics)

## Education and Certifications
- **Bachelor of Science (Cum Laude), Computer Science**<br />
  Clarke University, Dubuque, IA <br />
  May 2009
  - Cumulative GPA 3.63 (on a 4.00 scale)
  - Minor in music performance

- **Nagasaki University of Foreign Studies** (長崎外国語大学)<br />
  Nagasaki, Japan <br />
  April 2007 - August 2007
  - Study abroad experience via University of Wisconsin - Platteville

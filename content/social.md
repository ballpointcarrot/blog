+++
title = "/social/"
+++

Overall, I'm pushing to avoid major siloed social media systems. Instead, I'm putting my efforts into
supporting independent social media (often heard as the Fediverse and/or Indieweb).

Below are the locations that I use most frequently.

## General

* [@ballpointcarrot@fedi.bpc.wtf](https://fedi.bpc.wtf/@ballpointcarrot) - my main home for post-Twitter short form messaging.
* [@ballpointcarrot@oulipo.social](https://oulipo.social/@ballpointcarrot) - Similar to the top link, but avoids using 'E' in posts. Fun to pick your diction.
* [@ballpointcarrot@pixelfed.social](https://pixelfed.social/i/web/profile/499289855110833344) - it's like Instagram, but not owned by Facebook.

## Code 

* [Github](https://github.com/ballpointcarrot) - random projects and open source work
* [Gitlab](https://gitlab.com/ballpointcarrot) - random projects and open source work (less used, but want to invest more here)
* [dev.to](https://dev.to/ballpointcarrot) - long-form writing (mirrors this blog)
